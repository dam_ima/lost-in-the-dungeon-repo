﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider){
		collider.gameObject.GetComponent<SpriteRenderer> ().enabled = false;
	}

	void OnTriggerExit2D(Collider2D collider){
		collider.gameObject.GetComponent<SpriteRenderer> ().enabled = true;
	}

	void OnCollisionEnter2D(Collision2D collider) {
		SceneManager.LoadScene ("EscenaPrincipal", LoadSceneMode.Single);
	}
}
