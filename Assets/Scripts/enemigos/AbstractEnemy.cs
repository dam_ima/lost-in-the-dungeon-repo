﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IMAEnums;

public abstract class AbstractEnemy {

	public Elemento elementos { get; set;}
	protected Elemento enemyElem;
	public bool isPatrol;
	public int visionRange;
	public int attackRange;
	protected int health;
	protected int damage;

	protected abstract void recibeDamage (int damage, Elemento enemyElem);
	protected abstract void sendDamage (int damage, Elemento enemyElem);

}
