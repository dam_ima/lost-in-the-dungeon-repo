﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IMAEnums;

public class VisionRange : MonoBehaviour {

	EnemyController enemyController;
	BoxCollider2D boxCollider;
	// Use this for initialization

	void Start () {
		enemyController = gameObject.GetComponentInParent (typeof(EnemyController)) as EnemyController;
		boxCollider = gameObject.GetComponent<BoxCollider2D> ();
		if (enemyController.enemy.visionRange == EnemyController.MELEE_VISION_RANGE) {
			boxCollider.size = new Vector2 (boxCollider.size.x, boxCollider.size.y + EnemyController.MELEE_VISION_RANGE);
			boxCollider.offset = new Vector2 (boxCollider.offset.x,boxCollider.offset.y + (EnemyController.MELEE_VISION_RANGE/2));
		} else {
			boxCollider.size = new Vector2 (boxCollider.size.x, boxCollider.size.y + EnemyController.RANGER_VISION_RANGE);
			boxCollider.offset = new Vector2 (boxCollider.offset.x,boxCollider.offset.y + (EnemyController.RANGER_VISION_RANGE/2));
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		Debug.Log ("ENTRA TRIGGER VISION");
		if (collider.gameObject.CompareTag("Player")) {
			if (enemyController.state != Estado.atacar)
				enemyController.hasVision = true;
				enemyController.SendMessage ("perseguir");
		}
	}

	void OnTriggerStay2D(Collider2D collider){
		if (collider.gameObject.CompareTag("Player")) {
			if(enemyController.state != Estado.atacar){
				enemyController.hasVision = true;
				enemyController.SendMessage ("perseguir");
			}
		}
	}

	void OnTriggerExit2D(Collider2D collider){
		if (collider.gameObject.CompareTag("Player")) {
			enemyController.hasVision = false;
			enemyController.SendMessage ("perseguir");
		}
	}
}
