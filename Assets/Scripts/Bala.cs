﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {
	float time = 0;
	public float damage;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		transform.Translate (Vector2.right*Time.deltaTime*5);
		if (time >= 5) {
		
			Destroy (this.gameObject);
		
		}
	}

	void OnCollisionEnter2D(Collision2D collider) {
		if (!collider.gameObject.tag.Equals ("Player") && !collider.gameObject.tag.Equals("BulletAlly")) {
			Destroy (this.gameObject);
		}
	}
}
