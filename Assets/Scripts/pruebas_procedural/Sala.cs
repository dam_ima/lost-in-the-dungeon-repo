﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sala {

	public int x1;//coordenada x de inicio de la sala (lado izquierdo)
	public int x2;//coordenada x de final de la sala (lado derecho)
	public int y1;//coordenada y de inicio de la sala(lado inferior)
	public int y2;//coordenada y de final de la sala(lado superior)
	public Vector2 center;

	public Sala(int x1,int x2,int y1,int y2){
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		this.center = new Vector2(Mathf.Floor ((x1 + x2)/2),Mathf.Floor((y1+y2)/2));

	}

	public bool intersec(Sala sala){
		//si se superponen revisar y2 <= sala.y1

		Debug.Log("intersec: hay intersecion: " + ((x1 <= sala.x2 && x2 >= sala.x1 && y1 <= sala.y2 && y2 >= sala.y1) 
			|| (sala.x2 <= x1 && sala.x1 >= x2 && sala.y2 <= y1 && sala.y1 >= y2)));
		return ((x1 <= sala.x2 && x2 >= sala.x1 && y1 <= sala.y2 && y2 >= sala.y1) || (sala.x2 <= x1 && sala.x1 >= x2 && sala.y2 <= y1 && sala.y1 >= y2));
	}

}
