﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

public class BoardManager2 : MonoBehaviour
{
    public GameObject[] waterFloorTiles;
	public GameObject[] waterOuterWallsTiles;
	public GameObject[] waterWallsRoomsTiles;
	public GameObject[] waterFloorRoomsTiles;
	public GameObject[] waterTunelTiles;
	public GameObject[] waterCollectableTiles;
	public GameObject[] waterEnemiesTiles;

	public GameObject[] forestFloorTiles;
	public GameObject[] forestOuterWallsTiles;
	public GameObject[] forestWallsRoomsTiles;
	public GameObject[] forestFloorRoomsTiles;
	public GameObject[] forestTunelTiles;
	public GameObject[] forestCollectableTiles;
	public GameObject[] forestEnemiesTiles;

	public GameObject[] vulcanFloorTiles;
	public GameObject[] vulcanOuterWallsTiles;
	public GameObject[] vulcanWallsRoomsTiles;
	public GameObject[] vulcanFloorRoomsTiles;
	public GameObject[] vulcanTunelTiles;
	public GameObject[] vulcanCollectableTiles;
	public GameObject[] vulcanEnemiesTiles;

	public GameObject player;
	public GameObject[] bosses;
	private const int COLUMNS = 90;
	private const int FILAS = 90;
	private const int MAX_ENEMIES = 4;
	private const int MAX_COLLECT = 3;
	private int salaXMaxLen = 20;
	private int salaXMinLen = 15;
	private int salaYMaxLen = 20;
	private int salaYMinLen = 15;
	private int maxSalas = 4;
	private List<Sala> salas = new List<Sala> ();
	public GameObject[][] gridObject = new GameObject[FILAS][];
	private GameObject[] floorTiles;
	private GameObject[] outerWallsTiles;
	private GameObject[] wallsRoomsTiles;
	private GameObject[] floorRoomsTiles;
	private GameObject[] tunelTiles;
	private GameObject[] collectableTiles;
	private GameObject[] enemiesTiles;
	private bool mapDirectionChoise = false;
	private const int MIN_MARGIN_ROOM = 2;
	private const int MAX_MARGIN_ROOM = 6;
	private int mapOptionDirection = 0;
	private Transform boardHolder;
	// Use this for initialization

	private void initialiceGrids ()
	{
		salas.Clear ();
		for (int i = 0; i < FILAS; i++) {
			gridObject [i] = new GameObject[COLUMNS];
		}
	}

	private void initialiceGameAssets ()
	{
		
		int option = Random.Range (0, 3);
		if (option == 0) {
			floorTiles = waterFloorTiles;
			outerWallsTiles = waterOuterWallsTiles;
			wallsRoomsTiles = waterWallsRoomsTiles;
			floorRoomsTiles = waterFloorRoomsTiles;
			collectableTiles = waterCollectableTiles;
			enemiesTiles = waterEnemiesTiles;
			tunelTiles = waterTunelTiles;
		} else if (option == 1) {
			floorTiles = forestFloorTiles;
			outerWallsTiles = forestOuterWallsTiles;
			wallsRoomsTiles = forestWallsRoomsTiles;
			floorRoomsTiles = forestFloorRoomsTiles;
			collectableTiles = forestCollectableTiles;
			enemiesTiles = forestEnemiesTiles;
			tunelTiles = forestTunelTiles;
		} else if (option == 2) {
			floorTiles = vulcanFloorTiles;
			outerWallsTiles = vulcanOuterWallsTiles;
			wallsRoomsTiles = vulcanWallsRoomsTiles;
			floorRoomsTiles = vulcanFloorRoomsTiles;
			collectableTiles = vulcanCollectableTiles;
			enemiesTiles = vulcanEnemiesTiles;
			tunelTiles = vulcanTunelTiles;
		}
	}

	private Vector2 generateRandomPoint ()
	{
		int randomYIndex = Random.Range (0, gridObject.Length);
		int randomXIndex = Random.Range (0, gridObject [randomYIndex].Length);
		Vector2 choisePosition = new Vector2(0,0);

		if (!mapDirectionChoise) {
			mapDirectionChoise = true;
			Vector2 levelInitialPos = new Vector2 (0, 0);
			Vector2 levelFinalPosX = new Vector2 (gridObject [gridObject.Length-1].Length-1, 0);
			Vector2 levelFinalPosY = new Vector2 (0, gridObject.Length-1);
			Vector2 levelFinalPos = new Vector2 (gridObject [gridObject.Length-1].Length-1, gridObject.Length-1);
			Vector2[] cornerPositions = new Vector2[4];
			cornerPositions [0] = levelInitialPos;
			cornerPositions [1] = levelFinalPosY;
			cornerPositions [2] = levelFinalPosX;
			cornerPositions [3] = levelFinalPos;
			mapOptionDirection = Random.Range (0, cornerPositions.Length);
			choisePosition = cornerPositions [mapOptionDirection];

		} else {
			int nextRoomDirection = Random.Range (0,2);
			if (nextRoomDirection == 0) {
					choisePosition.y = salas [salas.Count - 1].y2;
					choisePosition.x = salas [salas.Count - 1].x1;
			} else {
				choisePosition.y = salas [salas.Count - 1].y1;
				choisePosition.x = salas [salas.Count - 1].x2;
			}
		}

		if (mapOptionDirection == 0) {
			choisePosition.x = choisePosition.x + Random.Range (MIN_MARGIN_ROOM, MAX_MARGIN_ROOM);
			choisePosition.y = choisePosition.y + Random.Range (MIN_MARGIN_ROOM, MAX_MARGIN_ROOM);
		} else if(mapOptionDirection == 1) {
			choisePosition.x = choisePosition.x + Random.Range (MIN_MARGIN_ROOM, MAX_MARGIN_ROOM);
			choisePosition.y = choisePosition.y - Random.Range (MIN_MARGIN_ROOM, MAX_MARGIN_ROOM);
		} else if(mapOptionDirection == 2) {
			choisePosition.x = choisePosition.x - Random.Range (MIN_MARGIN_ROOM, MAX_MARGIN_ROOM);
			choisePosition.y = choisePosition.y + Random.Range (MIN_MARGIN_ROOM, MAX_MARGIN_ROOM);
		} else {
			choisePosition.x = choisePosition.x - Random.Range (MIN_MARGIN_ROOM, MAX_MARGIN_ROOM);
			choisePosition.y = choisePosition.y - Random.Range (MIN_MARGIN_ROOM, MAX_MARGIN_ROOM);
		}
		Debug.Log ("generateRandomPoint: indice generado aleatorio yx: " + randomYIndex + " - " + randomXIndex);
		//Debug.Log("generateRandomPoint: Se ha generado una posicion aleatoria en:" + randomYIndex + " - " + randomXIndex);
		return choisePosition;
	}

	private Sala generateRoom ()
	{
		Vector2 startPosition = generateRandomPoint ();
		Vector2 posicionFinal = generateFinalPosition (startPosition);

		while (!posicionValida ((int)posicionFinal.x, (int)posicionFinal.y)) {
			startPosition = generateRandomPoint ();
			posicionFinal = generateFinalPosition (startPosition);
		}
		Debug.Log ("generateRoom: punto aleatorio: " + (int)startPosition.x + " - " + (int)posicionFinal.x + " - " + (int)startPosition.y + " - " + (int)posicionFinal.y);
		return new Sala ((int)startPosition.x, (int)posicionFinal.x, (int)startPosition.y, (int)posicionFinal.y);

	}

	private Vector2 generateFinalPosition(Vector2 startPosition){
		int roomLenX = Random.Range (salaXMinLen, salaXMaxLen + 1);
		int roomLenY = Random.Range (salaYMinLen, salaYMaxLen + 1);
		Vector2 posicionFinal = new Vector2 (0,0);

		if (mapOptionDirection == 0) {
			posicionFinal = new Vector2 (startPosition.x + roomLenX, startPosition.y + roomLenY);
		} else if(mapOptionDirection == 1) {
			posicionFinal = new Vector2 (startPosition.x + roomLenX, startPosition.y - roomLenY);
		} else if(mapOptionDirection == 2) {
			posicionFinal = new Vector2 (startPosition.x - roomLenX, startPosition.y + roomLenY);
		} else {
			posicionFinal = new Vector2 (startPosition.x - roomLenX, startPosition.y - roomLenY);
		}

		return posicionFinal;
	}

	private int hCorridor (Sala salaPrev, Sala salaNueva, int y = -1)
	{
		
		Vector2 minSala;
		Vector2 maxSala;

		if (Mathf.Min ((int)salaPrev.center.x, (int)salaNueva.center.x) == salaPrev.center.x) {
			minSala = new Vector2 (salaPrev.center.x, salaPrev.center.y);
			maxSala = new Vector2 (salaNueva.center.x, salaNueva.center.y);
		} else {
			maxSala = new Vector2 (salaPrev.center.x, salaPrev.center.y);
			minSala = new Vector2 (salaNueva.center.x, salaNueva.center.y);
		}
		int i;
		if (y == -1) {
			if (minSala.x == salaPrev.center.x) {
				y = (int)salaPrev.center.y;
			} else {
				y = (int)salaNueva.center.y;
			}
		}
		for (i = (int)minSala.x; i <= (int)maxSala.x; i++) {
			if (!( ((i > salaPrev.x1 && i < salaPrev.x2) && (y > salaPrev.y1 && y < salaPrev.y2)) ||
					((i < salaPrev.x1 && i > salaPrev.x2) && (y > salaPrev.y1 && y < salaPrev.y2)) ||
					((i > salaPrev.x1 && i < salaPrev.x2) && (y < salaPrev.y1 && y > salaPrev.y2)) ||
					((i < salaPrev.x1 && i > salaPrev.x2) && (y < salaPrev.y1 && y > salaPrev.y2)) ) &&
				!(( (i > salaNueva.x1 && i < salaNueva.x2) && (y > salaNueva.y1 && y < salaNueva.y2)) ||
					((i < salaNueva.x1 && i > salaNueva.x2) && (y > salaNueva.y1 && y < salaNueva.y2)) ||
					( (i > salaNueva.x1 && i < salaNueva.x2) && (y < salaNueva.y1 && y > salaNueva.y2)) ||
					((i < salaNueva.x1 && i > salaNueva.x2) && (y < salaNueva.y1 && y > salaNueva.y2)))) {
				GameObject toInstantiate = tunelTiles [Random.Range (0, tunelTiles.Length)];
				Destroy (gridObject [y] [i]);
				GameObject instance = Instantiate (toInstantiate, new Vector2 (i, y), Quaternion.identity) as GameObject;
				instance.transform.SetParent (this.boardHolder);
				gridObject [y] [i] = instance;
			}
		}
		return i -1;
	}

	private int vCorridor (Sala salaPrev, Sala salaNueva, int x = -1) {
		Vector2 minSala;
		Vector2 maxSala;

		if (Mathf.Min ((int)salaPrev.center.y, (int)salaNueva.center.y) == salaPrev.center.y) {
			minSala = new Vector2 (salaPrev.center.x, salaPrev.center.y);
			maxSala = new Vector2 (salaNueva.center.x, salaNueva.center.y);
		} else {
			maxSala = new Vector2 (salaPrev.center.x, salaPrev.center.y);
			minSala = new Vector2 (salaNueva.center.x, salaNueva.center.y);
		}
		int i;

		if (x == -1) {
			if (minSala.y == salaPrev.center.y) {
				x = (int)salaPrev.center.x;
			} else {
				x = (int)salaNueva.center.x;
			}
		}

		for (i = (int)minSala.y; i <= (int)maxSala.y; i++) {
			if (!( ((i > salaPrev.y1 && i < salaPrev.y2) && (x > salaPrev.x1 && x < salaPrev.x2)) ||
				((i < salaPrev.y1 && i > salaPrev.y2) && (x > salaPrev.x1 && x < salaPrev.x2)) ||
				((i > salaPrev.y1 && i < salaPrev.y2) && (x < salaPrev.x1 && x > salaPrev.x2)) ||
				((i < salaPrev.y1 && i > salaPrev.y2) && (x < salaPrev.x1 && x > salaPrev.x2)) ) &&
				!(( (i > salaNueva.y1 && i < salaNueva.y2) && (x > salaNueva.x1 && x < salaNueva.x2)) ||
					((i < salaNueva.y1 && i > salaNueva.y2) && (x > salaNueva.x1 && x < salaNueva.x2)) ||
					( (i > salaNueva.y1 && i < salaNueva.y2) && (x < salaNueva.x1 && x > salaNueva.x2)) ||
					((i < salaNueva.y1 && i > salaNueva.y2) && (x < salaNueva.x1 && x > salaNueva.x2)))) {
					GameObject toInstantiate = tunelTiles [Random.Range (0, tunelTiles.Length)];
					Destroy (gridObject [i] [x]);
					GameObject instance = Instantiate (toInstantiate, new Vector2 (x, i), Quaternion.identity) as GameObject;
					instance.transform.SetParent (this.boardHolder);
					gridObject [i] [x] = instance;
				}
		}

		return i -1;
	}

	private bool posicionValida (int finalX, int finalY)
	{
		if (finalY < gridObject.Length) {
			if (finalX < gridObject [finalY].Length)
				return true;
			else
				return false;
		} else
			return false;
	}

	private void generateRooms ()
	{
		bool isSalaNoInters;

		for (int i = 0; i < maxSalas; i++) {
			Sala salaNueva = generateRoom ();
			isSalaNoInters = false;

			while (!isSalaNoInters) {
				isSalaNoInters = true;
				for (int y = 0; y < salas.Count; y++) {
					if (salas [y].intersec (salaNueva)) {
						isSalaNoInters = false;
					}
				}
				if (!isSalaNoInters) {
					salaNueva = generateRoom ();
				}
			}
			Debug.Log ("generateRooms: añadida sala " + i + " en coordenadas:" + salaNueva.x1 + " - " + salaNueva.x2 + " - " + salaNueva.y1 + " - " +
			salaNueva.y2);
			salas.Add (salaNueva);
			pintarSala (salaNueva);
			Debug.Log ("generateRooms: Centro nueva sala es: " + salaNueva.center);
			if (salas.Count == maxSalas) {

				for (int z = 0; z < salas.Count - 1; z++) {
					//0 empieza por tunel vertical, 1 horizontal
					int corridorOption = Random.Range (0, 2);	
					if (corridorOption == 0) {
						if (salas [z].center.x == salas [z + 1].center.x) {
							vCorridor (salas [z], salas [z + 1]);
						} else {
							hCorridor (salas[z], salas[z+1], vCorridor (salas[z], salas[z+1]));
						}
					} else {
						if (salas [z].center.y == salas [z + 1].center.y) {
							hCorridor (salas [z], salas [z + 1]);
						} else {
							vCorridor (salas[z], salas[z+1], hCorridor (salas[z], salas[z+1]));	
						}
					}
				}
				//Sala prevSala = salas [i - 1];
			}
		}
	}

	private void pintarItemSala (int x, int y, GameObject[] tileArray)
	{
		GameObject toInstantiate = tileArray [Random.Range (0, tileArray.Length)];
		GameObject instance = Instantiate (toInstantiate, new Vector2 (x, y), Quaternion.identity) as GameObject;
		instance.transform.SetParent (this.boardHolder);
	}

	private void itemsToInstantiate(Sala sala){
		
		int rangeCollectables = Random.Range (0, MAX_COLLECT);
		int rangeEnemies = Random.Range (1, MAX_ENEMIES);
		int countCollect = 0;
		int countEnemies = 0;
		int randomChoice;
		int y1 = Math.Min (sala.y1, sala.y2);
		int y2 = Math.Max (sala.y1, sala.y2);
		int x1 = Math.Min (sala.x1, sala.x2);
		int x2 = Math.Max (sala.x1, sala.x2);
		List<Vector2> positions = new List<Vector2> ();
		bool isDone = false;
		for(int i = y1; i <= y2;i++) {
			for(int j = x1; j <= x2;j++){
				if (!(i == y1 || i == y2 || j == x1 || j == x2))
					positions.Add (new Vector2(j,i));
			}
		}

		while (!isDone) {
			Vector2 position = positions[Random.Range (0,positions.Count)];

			if (sala.center.Equals (salas [0].center)) {
				GameObject player = GameObject.FindGameObjectWithTag ("Player");

				if (player) {
					player.transform.position = new Vector3 (sala.center.x, sala.center.y, 0);
				} else {
					GameObject instance = Instantiate (this.player, new Vector2 (sala.center.x, sala.center.y), this.player.transform.rotation) as GameObject;
				}
				isDone = true;
			} else if(salas.Count == maxSalas) {
				GameObject toInstantiate = bosses [Random.Range (0, bosses.Length)];
				GameObject instance = Instantiate (toInstantiate, new Vector2 (sala.center.x, sala.center.y), Quaternion.identity) as GameObject;
				instance.transform.SetParent (this.boardHolder);
				instance.tag = "Boss";
				isDone = true;
			} else {
				if ((countEnemies < rangeEnemies || countCollect < rangeCollectables) || positions.Count == 0) {
					randomChoice = Random.Range (0, 2);
					if (randomChoice == 0 && rangeCollectables > countCollect) {
						countCollect++;
						pintarItemSala ((int)position.x, (int)position.y, collectableTiles);
						positions.Remove (position);
					} else if (randomChoice == 1 && rangeEnemies > countEnemies) {
						pintarItemSala ((int)position.x, (int)position.y, enemiesTiles);
						countEnemies++;
						positions.Remove (position);
					}
				} else {
					isDone = true;
				}	
			}
		}
	}

	private void pintarSala (Sala salaNueva)
	{
		GameObject toInstantiate;

		itemsToInstantiate (salaNueva);
		if (mapOptionDirection == 0) {
			for (int i = salaNueva.y1; i <= salaNueva.y2; i++) {
				for (int j = salaNueva.x1; j <= salaNueva.x2; j++) {
					if (i == salaNueva.y1 || i == salaNueva.y2 || j == salaNueva.x1 || j == salaNueva.x2) {
						toInstantiate = wallsRoomsTiles [Random.Range (0, wallsRoomsTiles.Length)];
						instanciarObjetoBoard (j,i,toInstantiate);
					} else {
						toInstantiate = floorRoomsTiles [Random.Range (0, floorRoomsTiles.Length)];
						instanciarObjetoBoard (j,i,toInstantiate);
					}
				}
			}
		} else if(mapOptionDirection == 1) {
			for (int i = salaNueva.y1; i >= salaNueva.y2; i--) {
				for (int j = salaNueva.x1; j <= salaNueva.x2; j++) {
					if (i == salaNueva.y1 || i == salaNueva.y2 || j == salaNueva.x1 || j == salaNueva.x2) {
						toInstantiate = wallsRoomsTiles [Random.Range (0, wallsRoomsTiles.Length)];
						instanciarObjetoBoard (j,i,toInstantiate);
					} else {
						toInstantiate = floorRoomsTiles [Random.Range (0, floorRoomsTiles.Length)];
						instanciarObjetoBoard (j,i,toInstantiate);
					}
				}
			}
		} else if(mapOptionDirection == 2) {
			for (int i = salaNueva.y1; i <= salaNueva.y2; i++) {
				for (int j = salaNueva.x1; j >= salaNueva.x2; j--) {
					if (i == salaNueva.y1 || i == salaNueva.y2 || j == salaNueva.x1 || j == salaNueva.x2) {
						toInstantiate = wallsRoomsTiles [Random.Range (0, wallsRoomsTiles.Length)];
						instanciarObjetoBoard (j,i,toInstantiate);
					} else {
						toInstantiate = floorRoomsTiles [Random.Range (0, floorRoomsTiles.Length)];
						instanciarObjetoBoard (j,i,toInstantiate);
					}
				}
			}
		} else {
			for (int i = salaNueva.y1; i >= salaNueva.y2; i--) {
				for (int j = salaNueva.x1; j >= salaNueva.x2; j--) {
					if (i == salaNueva.y1 || i == salaNueva.y2 || j == salaNueva.x1 || j == salaNueva.x2) {
						toInstantiate = wallsRoomsTiles [Random.Range (0, wallsRoomsTiles.Length)];
						instanciarObjetoBoard (j,i,toInstantiate);
					} else {
						toInstantiate = floorRoomsTiles [Random.Range (0, floorRoomsTiles.Length)];
						instanciarObjetoBoard (j,i,toInstantiate);
					}
				}
			}
		}
	}

	private void chooseRoomsPosition (int y, int x){
		
	}

	private void instanciarObjetoBoard(int x, int y, GameObject toInstantiate){
		GameObject instance = Instantiate (toInstantiate, new Vector2 (x, y), Quaternion.identity) as GameObject;
		instance.transform.SetParent (this.boardHolder);
		Destroy (gridObject [y] [x]);
		gridObject [y] [x] = instance;
	}
	private void boardSetup ()
	{
		GameObject toInstantiate;

		for (int i = 0; i < FILAS; i++) {
			for (int j = 0; j < COLUMNS; j++) {
				if (i == 0 || i == FILAS - 1 || j == 0 || j == COLUMNS - 1) {
					toInstantiate = outerWallsTiles [Random.Range (0, outerWallsTiles.Length)];
				} else {
					toInstantiate = floorTiles [Random.Range (0, floorTiles.Length)];
				}
				GameObject instance = Instantiate (toInstantiate, new Vector2 (j, i), Quaternion.identity) as GameObject;
				instance.transform.SetParent (this.boardHolder);
				gridObject [i] [j] = instance;
			}
		}
	}
	public void setupLevel(int level) {
		GameObject board = new GameObject ("Board");
		board.tag = "BoardManager";
		boardHolder = board.transform;
		mapDirectionChoise = false;
		initialiceGrids ();
		initialiceGameAssets ();
		boardSetup ();
		generateRooms ();
	}
}